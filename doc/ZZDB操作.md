## db = DB()
#### 创建数据库
code = 'SZ128053'
```
db.createTable(code, """
id INT UNSIGNED AUTO_INCREMENT not null,
ttime_stamp CHAR(20),
topen DOUBLE(14,3),
thigh DOUBLE(14,3),
tlow DOUBLE(14,3),
tclose DOUBLE(14,3),
tvolume DOUBLE(17,3),
tamount DOUBLE(17,3),
primary key (id)
""")
```
#### 插入数据
```
db.insertInto(code, "ttime_stamp, topen, thigh, tlow, tclose, tvolume, tamount", f"""{ttime_stamp}, {topen}, {thigh}, {tlow}, {tclose}, {tvolume}, {tamount}""")
```