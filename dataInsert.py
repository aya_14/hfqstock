'''
目的为创建新表、插入数据（新、旧）

根据time_stamp搜索最后一条
SELECT * FROM `sh110033` ORDER BY time_stamp DESC LIMIT 1;
类比当前数据，将新数据添加进来

1. 通达信下载最新数据到指定目录
2. 检视文件（倒序），与数据库内数据（最后一条）进行比对，将没加入数据插入到最后
'''

# import pymysql
import dataGet
import time

TABLE_NAME = "zz_day"
# 指标，对照新旧日期
AIMED_FILE = "SH113555.txt"
PATH = "C:\\Users\\LiuYuHao\\Desktop\\"
PATH_ZZ_contain_day = PATH + "ZZ_contain_day"
PATH_ZZ_contain_min = PATH + "ZZ_contain_min"
PATH_ZZ_day = PATH + "ZZ_day"
PATH_ZZ_min = PATH + "ZZ_min"
PATH_ARR = [PATH_ZZ_day, PATH_ZZ_min, PATH_ZZ_contain_day, PATH_ZZ_contain_min]

# DB
# 远程连接
# db = dataGet.dbConnect("192.168.0.102", 3306, "maclyh", "maclyh123456", TABLE_NAME)
# 本地连接
# db = dataGet.dbConnect("localhost", 3306, "root", "123456", TABLE_NAME)


def dayDataAnalysis(path, filename):
    tpath = path + "\\" + filename
    # utf-8, ISO-8859-1
    with open(tpath, 'r', encoding='gbk') as f:
        tda = []
        rls = f.readlines()
        for nrl in rls[2:-1]:
            # nrl: ['2020/03/12', '0931', '298.100', '374.977', '298.100', '374.977', '31017', '113202552.00']
            nrl = nrl.strip().split('\t')
            ts = int(time.mktime(time.strptime(nrl[:1][0], "%Y/%m/%d")))
            del nrl[:1]
            nrl.insert(0, ts)
            tda.append(nrl)
        return tda


# dayDataAnalysis(PATH_ZZ_day, AIMED_FILE)

with open(PATH_ZZ_day + "\\" + AIMED_FILE, 'r', encoding='gbk') as f:
    rls = f.readlines()
    # 每日下载的最新文件
    lastLine = rls[-2:-1]
    # ['2020/07/21\t550.00\t628.50\t545.01\t622.92\t773500\t4583606784.00\n']
    d = rls[2:-1]
    for nrl in d[::-1]:
        print(nrl)
        nrl = nrl.strip().split('\t')


# with db.cursor() as cur:
#     sql = f"INSERT INTO {TABLE_NAME} (time_stamp, open, high, low, close, volume, amount) VALUES ()"
#     try:
#         cur.execute(sql)
#         db.commit()
#         print("执行：" + sql + " 成功")
#     except Exception:
#         print("发生异常", Exception)
#         db.rollback()
