# 此类很初级，功能很简单，so...do not care
# 慢慢更新
import pymysql


class DB():
    def __init__(self, host, port, user, password, db):
        self.conndb = pymysql.connect(
            host=host,
            port=port,
            user=user,
            password=password,
            db=db,
            charset='utf8'
        )
        self.cursor = self.conndb.cursor()

    def __enter__(self):
        return self

    def __exit__(self, type, value, trace):
        # self.cursor.close()
        # self.conndb.close()
        pass

    # --- 方法 ---
    # db.dbVersion()
    def dbVersion(self):
        with self as dbcursor:
            dbcursor.cursor.execute("SELECT VERSION()")
            data = dbcursor.cursor.fetchone()
            print("Database version : %s " % data)

    # db = DB()
    # db.createTable("STUDENT", """
    # id INT AUTO_INCREMENT NOT NULL,
    # fn CHAR(20) NOT NULL,
    # ln CHAR(20) NOT NULL,
    # gender CHAR(1),
    # age INT,
    # primary key (id)
    # """)
    def createTable(self, tableName, sqlArgs):
        with self as dbcursor:
            tsql = f"DROP TABLE IF EXISTS {tableName}"
            dbcursor.cursor.execute(tsql)
            sql_sentences = ""
            for s in sqlArgs.split(","):
                sql_sentences = sql_sentences + s.strip() + ","
            tsql = f"CREATE TABLE {tableName} ({sql_sentences[:-1]})"
            try:
                dbcursor.cursor.execute(tsql)
                print("执行：" + tsql + " 成功")
            except BaseException:
                print("创建失败")

    # db = DB()
    # db.insertInto("STUDENT", "fn, ln, gender, age", '''"liu", "yu", "M", 20''')
    def insertInto(self, tableName, keyArgs, valueArgs):
        with self as dbcursor:
            tsql = f"INSERT INTO {tableName} ({keyArgs}) VALUES ({valueArgs})"
            try:
                dbcursor.cursor.execute(tsql)
                dbcursor.conndb.commit()
                print("执行：" + tsql + " 成功")
            except Exception:
                print("发生异常", Exception)
                dbcursor.conndb.rollback()

    # db = DB()
    # d = db.selectTable(selectArgs="*", fromArgs="STUDENT", whereArgs="")
    # if d is not None:
    #     for i in d:
    #         print("id:", i[0])
    # else:
    #     print("d is NONE")
    """
    1. offset比较小的时候。
    随意
    2. offset大的时候。
    # select * from yanxue8_visit limit 10000,10
    多次运行，时间保持在0.0187左右

    Select * From yanxue8_visit Where vid >=(
        Select vid From yanxue8_visit Order By vid limit 10000,1
    ) limit 10
    多次运行，时间保持在0.0061左右，只有前者的1/3。
    """
    # 执行顺序
    # select –> where –> group by –> having –> order by
    # limit offset 要么limit 0,10（从第0+1行开始数10行）单独用，要么limit 10（数10行）、offset 0（从第0+1行开始，默认为0）一起用
    def selectTable(self, selectArgs, fromArgs, whereArgs, limit=1000, offset=0):
        with self as dbcursor:
            tsql = f"SELECT {selectArgs} FROM {fromArgs}"
            if whereArgs:
                tsql += f" WHERE {whereArgs}"
            # LIMIT OFFSET 有次序的
            if limit:
                tsql += f" LIMIT {limit}"
            if offset:
                tsql += f" OFFSET {offset}"
            try:
                dbcursor.cursor.execute(tsql)
                data = dbcursor.cursor.fetchall()
                print("执行：" + tsql + " 成功")
                return data
            except BaseException:
                dbcursor.conndb.rollback()

    # db.updateTable(tableName="STUDENT", setArgs="fn='hao'", whereArgs="age=21")
    def updateTable(self, tableName, setArgs, whereArgs):
        with self as dbcursor:
            tsql = f"UPDATE {tableName} SET {setArgs}"
            if whereArgs:
                tsql += f"WHERE {whereArgs}"
            try:
                dbcursor.cursor.execute(tsql)
                dbcursor.conndb.commit()
                print("执行：" + tsql + " 成功")
            except BaseException:
                dbcursor.conndb.rollback()

    def deleteTable(self, tableName, whereArgs):
        with self as dbcursor:
            tsql = f"DELETE FROM {tableName} WHERE {whereArgs}"
            try:
                dbcursor.cursor.execute(tsql)
                dbcursor.conndb.commit()
                print("执行：" + tsql + " 成功")
            except BaseException:
                dbcursor.conndb.rollback()

    # db.executeSQL("""INSERT INTO STUDENT (fn, ln, gender, age) VALUES ('liu', 'hao', 'M', 20)""")
    # def executeSQL(self, sql):
    #     with self as dbcursor:
    #         tsql = sql
    #         try:
    #             dbcursor.cursor.execute(tsql)
    #             dbcursor.conndb.commit()
    #             print("执行：" + tsql + " 成功")
    #         except BaseException:
    #             dbcursor.conndb.rollback()
