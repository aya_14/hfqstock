import requests
import json
import time
import os
import dataPath


DFCF_ZZ_URL_TOKEN = "70f12f2f4f091e459a279469fe49eca5"

ZZ_FOLDER = dataPath.PATH + 'zz_msg//'


def paDFCFZZMsg():
    # 爬取东方财富转债信息
    with open("zztostock.json", "r") as f:
        j = json.load(f)
        # zz: 110031
        for zz in j:
            zzurl = f"http://dcfm.eastmoney.com/em_mutisvcexpandinterface/api/js/get?type=KZZ_LB&token={DFCF_ZZ_URL_TOKEN}&filter=(BONDCODE=%27{zz}%27)&js=var%20KZZ_LB=[(x)]"
            r = requests.get(zzurl)
            with open(f"{ZZ_FOLDER}{zz}.json", "w", encoding='utf-8') as w:
                w.write(r.text)
                print(f'{zz}.json ok')
                w.close()
            time.sleep(3)


def cleanData():
    for files in os.listdir(ZZ_FOLDER):
        zz_file_path = f'{ZZ_FOLDER}/{files}'
        with open(zz_file_path, 'r', encoding='utf-8') as f:
            content = f.read()
            content = content.strip()
            # 截取内容，替换\r\n
            content = content[13:-2].replace('\\r\\n', '')
            f.close()
            with open(zz_file_path, 'w', encoding='utf-8') as w:
                w.write(content)
                w.close()
        try:
            json.loads(zz_file_path)
            print(f'{files} 符合json')
        except Exception as e:
            print(f'{files} 不符合json: {e}')


def workFlow():
    # 1. 爬取最新各转债信息
    paDFCFZZMsg()
    cleanData()
    # 2. 
