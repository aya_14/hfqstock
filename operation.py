'''
统计可转债收益
'''
# import requests
# import re
# import ast
import time
import os
from hfqdb import DB

PATH_ZZ_contain_day = "M:\\DB\\ZZ_contain_day"
PATH_ZZ_contain_min = "M:\\DB\\ZZ_contain_min"
PATH_ZZ_day = "M:\\DB\\ZZ_day"
PATH_ZZ_min = "M:\\DB\\ZZ_min"


def dayDataAnalysis(path, filename):
    tpath = path + "\\" + filename
    # utf-8, ISO-8859-1
    with open(tpath, 'r', encoding='gbk') as f:
        tda = []
        rls = f.readlines()
        for nrl in rls[2:-1]:
            # nrl: ['2020/03/12', '0931', '298.100', '374.977', '298.100', '374.977', '31017', '113202552.00']
            nrl = nrl.strip().split('\t')
            ts = int(time.mktime(time.strptime(nrl[:1][0], "%Y/%m/%d")))
            del nrl[:1]
            nrl.insert(0, ts)
            tda.append(nrl)
        return tda


def minDataAnalysis(path, filename):
    tpath = path + filename
    # utf-8, ISO-8859-1
    with open(tpath, 'r', encoding='gbk') as f:
        tda = []
        rls = f.readlines()
        for nrl in rls[:-1]:
            # nrl: ['2020/03/12', '0931', '298.100', '374.977', '298.100', '374.977', '31017', '113202552.00']
            nrl = nrl.strip().split('\t')
            ts = strToMinTimestamp(nrl[:2])
            del nrl[:2]
            nrl.insert(0, ts)
            tda.append(nrl)
        f.close()
        return tda


# 因为数据 t = ['2020/03/12', '0931'] 就是这样，单位 s
def strToMinTimestamp(strArr):
    t = strArr[1][:2] + ':' + strArr[1][2:] + ":00"
    st = strArr[0] + ' ' + t
    ta = time.strptime(st, "%Y/%m/%d %H:%M:%S")
    timestamp = int(time.mktime(ta))
    return timestamp


# ----------------------------
def zz_create():
    db = DB("localhost", 3306, "root", "123456", "zz_contain_day")
    code = ''
    files = os.listdir(PATH_ZZ_contain_day)
    for filename in files:
        code = filename
        db.createTable(code[:-4], """
        id INT UNSIGNED AUTO_INCREMENT not null primary key,
        time_stamp CHAR(20),
        open DOUBLE(14,3),
        high DOUBLE(14,3),
        low DOUBLE(14,3),
        close DOUBLE(14,3),
        volume DOUBLE(17,3),
        amount DOUBLE(17,3)
        """)
        # [1583976660, '298.100', '374.977', '298.100', '374.977', '31017', '113202552.00']
        # 这都是一分钟内的数据
        # [时间，开始价，最高，最低，终止价，成交量，成交金额]
        da = dayDataAnalysis(PATH_ZZ_contain_day, code)
        for tda in da:
            t_time_stamp = tda[0]
            t_open = tda[1]
            t_high = tda[2]
            t_low = tda[3]
            t_close = tda[4]
            t_volume = tda[5]
            t_amount = tda[6]
            db.insertInto(code[:-4], "time_stamp, open, high, low, close, volume, amount", f"""{t_time_stamp}, {t_open}, {t_high}, {t_low}, {t_close}, {t_volume}, {t_amount}""")


def main():
    zz_create()


if __name__ == '__main__':
    main()
